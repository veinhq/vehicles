public class Vehicle {

    private  String typ;
    private  String producent;
    private  int MaxSpeed;

    public Vehicle(String typ, String producent, int MaxSpeed) {
        this.typ = typ;
        this.producent = producent;
        this.MaxSpeed = MaxSpeed;
    }

    public String getTyp (){
        return typ;
    }

    public String getProducent() {
        return producent;
    }

    public int getMaxSpeed() {
        return MaxSpeed;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public void setProducent(String producent) {
        this.producent = producent;
    }

    public void setMaxSpeed(int maxSpeed) {
        MaxSpeed = maxSpeed;
    }


}
