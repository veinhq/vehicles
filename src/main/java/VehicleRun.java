import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class VehicleRun {

    private static final Logger logger = LoggerFactory.getLogger(VehicleRun.class);
    Scanner Sc = new Scanner(System.in);

    List<Vehicle> Vehicless = new ArrayList<>();

    private Vehicle TheFastest(String type) {

        List<Vehicle> lista;
        if (Objects.equals(type, "ALL")) {
            lista = Vehicless;
        } else {
            lista = allVehicles(type);
        }

        Vehicle fastest = lista.get(0);

        for (int i = 1; i < lista.size(); i++) {
            if (lista.get(i).getMaxSpeed() > fastest.getMaxSpeed()) {
                fastest = lista.get(i);
            }
        }
        return fastest;
    }

    private List<Vehicle> allVehicles(String type) {

        List<Vehicle> drive = new ArrayList<>();
        for (Vehicle vehicless : Vehicless) {
            if (Objects.equals(vehicless.getTyp(), type)) {
                drive.add(vehicless);
            }
        }
        return drive;
    }

    public void Run() {
        Vehicless.add(new Vehicle("CAR", "VW", 270));
        Vehicless.add(new Vehicle("CAR", "BMW", 310));
        Vehicless.add(new Vehicle("SHIP", "Motorboat", 180));
        Vehicless.add(new Vehicle("SHIP", "Transport ship", 90));
        Vehicless.add(new Vehicle("PLANE", "War plane", 220));
        Vehicless.add(new Vehicle("PLANE", "Jet", 450));
        Vehicless.add(new Vehicle("BICYCLE", "Cross", 50));
        Vehicless.add(new Vehicle("BICYCLE", "BMX", 30));

        while (true) {
            System.out.println("CAR, SHIP, PLANE, BICYCLE, ALL, EXIT");
            String Word = Sc.nextLine();
            Word = Word.toUpperCase(Locale.ROOT);

            switch (Word) {
                case "CAR":
                case "SHIP":
                case "PLANE":
                case "BICYCLE":
                    Vehicle vehicle = TheFastest(Word);
                    logger.info(" Pojazd " + vehicle.getTyp() +
                            " producenta " + vehicle.getProducent() +
                            " jest najszybszy (maksymalna prędkość to= " + vehicle.getMaxSpeed() + "km/h)");
                    break;
                case "ALL":
                    Vehicle vehicle1 = TheFastest(Word);
                    logger.info("Najszybszy pojazd ze wszystkich to : " + vehicle1.getTyp() +
                            " Producenta: " + vehicle1.getProducent() +
                            " którego prędkość to: " + vehicle1.getMaxSpeed() + "km/h");
                    break;
                case "EXIT":
                    System.exit(0);
                    break;
                default:
                    logger.info("Podaj prawidłową opcję");

            }
        }
    }
}